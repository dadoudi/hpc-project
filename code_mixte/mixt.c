#include "projet.h"
#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>
#include <time.h>	/* chronometrage */
#include <sys/time.h>

int rang, p;
MPI_Status status;
unsigned long long int node_searched = 0;
MPI_Datatype mpi_tree;
MPI_Datatype mpi_result;

void evaluate_seq(tree_t * T, result_t *result, int nb_appel)
{
  node_searched++;
  
  move_t moves[MAX_MOVES];
  int n_moves;
  
  result->score = -MAX_SCORE - 1;
  result->pv_length = 0;
  
  if (test_draw_or_victory(T, result))
    return;
  
  if (TRANSPOSITION_TABLE && tt_lookup(T, result))     /* la réponse est-elle déjà connue ? */
    return;
  
  compute_attack_squares(T);
  
  /* profondeur max atteinte ? si oui, évaluation heuristique */
  if (T->depth == 0) {
    result->score = (2 * T->side - 1) * heuristic_evaluation(T);
    return;
  }
  
  n_moves = generate_legal_moves(T, &moves[0]);
  
  /* absence de coups légaux : pat ou mat */
  if (n_moves == 0) {
    result->score = check(T) ? -MAX_SCORE : CERTAIN_DRAW;
    return;
  }
  
  if (ALPHA_BETA_PRUNING)
    sort_moves(T, n_moves, moves);
  
  /* évalue récursivement les positions accessibles à partir d'ici */

#pragma ompt parallel
#pragma omp for
  for (int i = 0; i < n_moves; i++) {
    tree_t child;
    result_t child_result;
                
    play_move(T, moves[i], &child);
    
    evaluate_seq(&child, &child_result, nb_appel+1);
    
    
    int child_score = -child_result.score;
#pragma omp critcal
    {
    if (child_score > result->score) {
      result->score = child_score;
      result->best_move = moves[i];
      result->pv_length = child_result.pv_length + 1;
      for(int j = 0; j < child_result.pv_length; j++)
	result->PV[j+1] = child_result.PV[j];
      result->PV[0] = moves[i];
    }
    }
    
    /*if (ALPHA_BETA_PRUNING && child_score >= T->beta)
      break;    */
    
    T->alpha = MAX(T->alpha, child_score);
  }
  
  if (TRANSPOSITION_TABLE)
    tt_store(T, result);
}


void recevoir_resultat(int source, result_t * result) {
  MPI_Recv( &(result->score), 1, MPI_INT, source, 10, MPI_COMM_WORLD, &status );
  MPI_Recv( &(result->best_move), 1, MPI_INT, source, 10, MPI_COMM_WORLD, &status );
  MPI_Recv( &(result->pv_length), 1, MPI_INT, source, 10, MPI_COMM_WORLD, &status );
  MPI_Recv( &(result->PV), MAX_DEPTH, MPI_INT, source, 10, MPI_COMM_WORLD, &status );
}

void envoyer_resultat(result_t * result) {
  MPI_Send( &(result->score), 1, MPI_INT, 0, 10, MPI_COMM_WORLD );
  MPI_Send( &(result->best_move), 1, MPI_INT, 0, 10, MPI_COMM_WORLD );
  MPI_Send( &(result->pv_length), 1, MPI_INT, 0, 10, MPI_COMM_WORLD );
  MPI_Send( &(result->PV), MAX_DEPTH, MPI_INT, 0, 10, MPI_COMM_WORLD );
}

void evaluate(tree_t * T, result_t *result)
{
  if ( rang == 0) {

    node_searched++;
  
    move_t moves[MAX_MOVES];
    int n_moves;
    int j = 1;
    result_t* buffer = NULL;
    int pp = 0;
    int* esclave; // pour savoir quel esclave fait quelle tache, on en a besoin pour recuperer moves[i]
    
    result->score = -MAX_SCORE - 1;
    result->pv_length = 0;
    
    /*if (test_draw_or_victory(T, result))
      printf("victoire ? \n");
    return;
      
    if (TRANSPOSITION_TABLE && tt_lookup(T, result))   
      printf("victoire ? \n");
      return;*/

    compute_attack_squares(T);
  
    /* profondeur max atteinte ? si oui, évaluation heuristique */

    if (T->depth == 0) {
      result->score = (2 * T->side - 1) * heuristic_evaluation(T);
      return;
    }
  
    n_moves = generate_legal_moves(T, &moves[0]);
    
    buffer = malloc(n_moves * sizeof(result_t));
    esclave = malloc(n_moves * sizeof(int));

    for(int k = 0; k < n_moves; k++) {
      esclave[k] = -1;
    }
  
    /* absence de coups légaux : pat ou mat */
    if (n_moves == 0) {
      result->score = check(T) ? -MAX_SCORE : CERTAIN_DRAW;
      return;
    }
  
    if (ALPHA_BETA_PRUNING)
      sort_moves(T, n_moves, moves);
    
    /* évalue récursivement les positions accessibles à partir d'ici */
#pragma omp parallel
#pragma omp for
    for (int i = 0; i < n_moves; i++) {
      tree_t child;
      result_t child_result;
      play_move(T, moves[i], &child);
#pragma omp critical
	   {
      if( j < p ) {
	esclave[i] = j;
	MPI_Send( &child, 1, mpi_tree, j, 10, MPI_COMM_WORLD );
	j++;
      } else {
	recevoir_resultat(MPI_ANY_SOURCE, &child_result);
	int source = status.MPI_SOURCE;
	// selon la source, voir ou est-ce qu'on doit stocker
	for(int k = 0; k < n_moves; k++) {
	  if( esclave[k] == source ) {
	    pp = k;
	    break;
	  }
	}

	buffer[pp] = child_result;
	esclave[pp] = -1;
	
       
	esclave[i] = source;
	MPI_Send( &child, 1, mpi_tree, source , 10, MPI_COMM_WORLD );
       
	 }
      }
    } // fin for

    // Quand il n'y a plus de taches a attribuer

    for(int i = 0; i < n_moves; i++ ) {
      if(esclave[i] != -1) {
	recevoir_resultat(esclave[i], &(buffer[i]));	
      }
    }
    
    for( int i = 1; i < p; i++ ) {
      MPI_Send(NULL, 0, mpi_tree, i, 0, MPI_COMM_WORLD);  
    }

    for( int i = 0; i < n_moves; i++ ) {
      result_t child_result = buffer[i];
      int child_score = -child_result.score;

      if (child_score > result->score) {
	result->score = child_score;
	result->best_move = moves[i];
	result->pv_length = child_result.pv_length + 1;
	for(int j = 0; j < child_result.pv_length; j++)
	  result->PV[j+1] = child_result.PV[j];
	result->PV[0] = moves[i];
      }

      /*if (ALPHA_BETA_PRUNING && child_score >= T->beta)
	break;*/

	T->alpha = MAX(T->alpha, child_score);
    }

  
    printf("depth: %d / score: %.2f / best_move : ", T->depth, 0.01 * result->score);
    print_pv(T,result);
    // a remettre !!
    /*if (test_draw_or_victory(T, result))
      printf("victoire ? \n");
      return;
  
    if (TRANSPOSITION_TABLE && tt_lookup(T, result))   
      printf("victoire ? \n");
      return;*/
  
    if (TRANSPOSITION_TABLE)
      tt_store(T, result);

    free(buffer);
    free(esclave);
  }

  else { // Esclave
    evaluate_seq( T, result, 0 );
    envoyer_resultat(result);
  }
}


void decide(tree_t * T, result_t *result)
{
  for (int depth = 1;; depth++) {
    T->depth = depth;
    T->height = 0;
    T->alpha_start = T->alpha = -MAX_SCORE - 1;
    T->beta = MAX_SCORE + 1;
    
    printf("=====================================\n");
    
    evaluate(T, result);
    
    printf("depth: %d / score: %.2f / best_move : ", T->depth, 0.01 * result->score);
    print_pv(T, result);
    
    if (DEFINITIVE(result->score))
      break;
  }
}

double my_gettimeofday(){
  struct timeval tmp_time;
  gettimeofday(&tmp_time, NULL);
  return tmp_time.tv_sec + (tmp_time.tv_usec * 1.0e-6L);
}


int main(int argc, char *argv[]) {
  tree_t root;
  result_t result;
  
   /* Init */

  MPI_Init(&argc,&argv);
  MPI_Comm_size(MPI_COMM_WORLD, &p);
  MPI_Comm_rank(MPI_COMM_WORLD,&rang);

  /* Creation des types MPI */

  // Type arbre

  int t_blocklen[5] = { 128*2, 10, 128, 1 , MAX_DEPTH+1};
  MPI_Datatype t_old_types[5] = {  MPI_CHAR, MPI_INT, MPI_CHAR, MPI_INT, MPI_UNSIGNED_LONG };
  
  MPI_Aint t_indice[5], extent_char, extent_int, lower_bound_int, lower_bound_char;

  MPI_Type_get_extent( MPI_CHAR, &lower_bound_int, &extent_char );
  MPI_Type_get_extent( MPI_INT, &lower_bound_char, &extent_int );
  
  t_indice[0] = 0;
  t_indice[1] = 128 * 2 * extent_char;
  t_indice[2] = t_indice[1] + 10 * extent_int;
  t_indice[3] = t_indice[2] + 128 * extent_char;
  t_indice[4] = t_indice[3] + extent_int; 

  MPI_Type_create_struct( 4, t_blocklen, t_indice, t_old_types, &mpi_tree );
  MPI_Type_commit( &mpi_tree );

  // Type result

  int r_blocklen[1] = { MAX_DEPTH+3 };
  MPI_Datatype r_old_types[1] = { MPI_INT };

  MPI_Aint r_indice[1];

  r_indice[0] = 0;

  MPI_Type_create_struct( 1, r_blocklen, r_indice, r_old_types, &mpi_result );
  MPI_Type_commit( &mpi_result );

  if( rang == 0) {
    // Maitre

    /* Chronometrage */
    double debut, fin;

    //debut du chronometrage 
    debut = my_gettimeofday();
  
    if (argc < 2) {
      printf("usage: %s \"4k//4K/4P w\" (or any position in FEN)\n", argv[0]);
      exit(1);
    }
    
    if (ALPHA_BETA_PRUNING)
    printf("Alpha-beta pruning ENABLED\n");
    
    if (TRANSPOSITION_TABLE) {
      printf("Transposition table ENABLED\n");
      init_tt();
    }
    
    parse_FEN(argv[1], &root);
    print_position(&root);
    printf("=====================================\n");

    // Init arbre (qui est normalement dans decide)
    root.depth = 10;
    root.height = 0;
    root.alpha_start = root.alpha = -MAX_SCORE - 1;
    root.beta = MAX_SCORE + 1;

    evaluate(&root, &result);
    
    printf("\nDécision de la position: ");
    switch(result.score * (2*root.side - 1)) {
    case MAX_SCORE: printf("blanc gagne\n"); break;
    case CERTAIN_DRAW: printf("partie nulle\n"); break;
    case -MAX_SCORE: printf("noir gagne\n"); break;
    default: printf("BUG\n");
    }
    
    printf("Node searched: %llu\n", node_searched);
    /* fin du chronometrage */
    fin = my_gettimeofday();
    fprintf( stderr, "Temps total de calcul : %g sec\n",  fin - debut);
    fprintf( stdout, "%g\n", fin - debut);
   
    if (TRANSPOSITION_TABLE)
      free_tt();

  } else {

    // Esclave
    // reçoit l'arbre qu'il doit traiter du maître
    while(1) {   
      MPI_Recv( &root, 1, mpi_tree, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status );     
      if( status.MPI_TAG != 0 ) {
	evaluate( &root, &result );
      } else {
	break;
      }
    }
  }

  MPI_Type_free( &mpi_result );
  MPI_Type_free( &mpi_tree );
  MPI_Finalize();
  return 0;
}
