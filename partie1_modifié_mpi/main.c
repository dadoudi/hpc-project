#include "projet.h"
#include <time.h>	/* chronometrage */
#include <sys/time.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

/* Avril - Mai 2017 */

int rang, p, np;
MPI_Status status;
unsigned long long int node_searched = 0;

void recevoir_resultat(int source, result_t * result) {
  MPI_Recv( &(result->score), 1, MPI_INT, source, 10, MPI_COMM_WORLD, &status );
  MPI_Recv( &(result->best_move), 1, MPI_INT, source, 10, MPI_COMM_WORLD, &status );
  MPI_Recv( &(result->pv_length), 1, MPI_INT, source, 10, MPI_COMM_WORLD, &status );
  MPI_Recv( &(result->PV), MAX_DEPTH, MPI_INT, source, 10, MPI_COMM_WORLD, &status );
}

void envoyer_resultat(result_t * result) {
  MPI_Send( &(result->score), 1, MPI_INT, 0, 10, MPI_COMM_WORLD );
  MPI_Send( &(result->best_move), 1, MPI_INT, 0, 10, MPI_COMM_WORLD );
  MPI_Send( &(result->pv_length), 1, MPI_INT, 0, 10, MPI_COMM_WORLD );
  MPI_Send( &(result->PV), MAX_DEPTH, MPI_INT, 0, 10, MPI_COMM_WORLD );

}

void recevoir_arbre(tree_t * tree) {

  if ( tree == NULL ) {
    MPI_Recv( &(tree->side), 1, MPI_INT, 0, 0, MPI_COMM_WORLD, &status );
  } else {
    MPI_Recv( &(tree->pieces), 128, MPI_CHAR, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status );
    MPI_Recv( &(tree->colors), 128, MPI_CHAR, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status );
    MPI_Recv( &(tree->side), 1, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status );
    MPI_Recv( &(tree->depth), 1, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status );
    MPI_Recv( &(tree->height), 1, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status );
    MPI_Recv( &(tree->alpha), 1, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status );
    MPI_Recv( &(tree->beta), 1, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status );
    MPI_Recv( &(tree->alpha_start), 1, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status );
    MPI_Recv( &(tree->king), 2, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status );
    MPI_Recv( &(tree->pawns), 2, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status );
    MPI_Recv( &(tree->attack), 128, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status );
    MPI_Recv( &(tree->suggested_move), 1, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status );
  }


}

void envoyer_arbre(tree_t * tree, int dest, int tag) {
  if ( tree == NULL ) {
    int i = 4;
    MPI_Send(&i, 1, MPI_INT, dest, tag, MPI_COMM_WORLD );
  } else {
    MPI_Send( &(tree->pieces), 128, MPI_CHAR, dest, tag, MPI_COMM_WORLD );
    MPI_Send( &(tree->colors), 128, MPI_CHAR, dest, tag, MPI_COMM_WORLD );
    MPI_Send( &(tree->side), 1, MPI_INT, dest, tag, MPI_COMM_WORLD );
    MPI_Send( &(tree->depth), 1, MPI_INT, dest, tag, MPI_COMM_WORLD );
    MPI_Send( &(tree->height), 1, MPI_INT, dest, tag, MPI_COMM_WORLD );
    MPI_Send( &(tree->alpha), 1, MPI_INT, dest, tag, MPI_COMM_WORLD );
    MPI_Send( &(tree->beta), 1, MPI_INT, dest, tag, MPI_COMM_WORLD );
    MPI_Send( &(tree->alpha_start), 1, MPI_INT, dest, tag, MPI_COMM_WORLD );
    MPI_Send( &(tree->king), 2, MPI_INT, dest, tag, MPI_COMM_WORLD );
    MPI_Send( &(tree->pawns), 2, MPI_INT, dest, tag, MPI_COMM_WORLD );
    MPI_Send( &(tree->attack), 128, MPI_INT, dest, tag, MPI_COMM_WORLD );
    MPI_Send( &(tree->suggested_move), 1, MPI_INT, dest, tag, MPI_COMM_WORLD );
  }
}

void evaluate_seq(tree_t * T, result_t *result)
{
  node_searched++;
  
  move_t moves[MAX_MOVES];
  int n_moves;

  result->score = -MAX_SCORE - 1;
  result->pv_length = 0;
        
  if (test_draw_or_victory(T, result))
    return;

  if (TRANSPOSITION_TABLE && tt_lookup(T, result))     /* la réponse est-elle déjà connue ? */
    return;
        
  compute_attack_squares(T);

  /* profondeur max atteinte ? si oui, évaluation heuristique */
  if (T->depth == 0) {
    result->score = (2 * T->side - 1) * heuristic_evaluation(T);
    return;
  }
        
  n_moves = generate_legal_moves(T, &moves[0]);

  /* absence de coups légaux : pat ou mat */
  if (n_moves == 0) {
    result->score = check(T) ? -MAX_SCORE : CERTAIN_DRAW;
    return;
  }
        
  if (ALPHA_BETA_PRUNING)
    sort_moves(T, n_moves, moves);

  /* évalue récursivement les positions accessibles à partir d'ici */
  for (int i = 0; i < n_moves; i++) {
    tree_t child;
    result_t child_result;
                
    play_move(T, moves[i], &child);
                
    evaluate_seq(&child, &child_result);
                         
    int child_score = -child_result.score;

    if (child_score > result->score) {
      result->score = child_score;
      result->best_move = moves[i];
      result->pv_length = child_result.pv_length + 1;
      for(int j = 0; j < child_result.pv_length; j++)
	result->PV[j+1] = child_result.PV[j];
      result->PV[0] = moves[i];
    }

    if (ALPHA_BETA_PRUNING && child_score >= T->beta)
      break;    

    T->alpha = MAX(T->alpha, child_score);
  }

  if (TRANSPOSITION_TABLE)
    tt_store(T, result);
}


void evaluate(tree_t * T, result_t *result)
{

  if ( rang == 0 ) {
    node_searched++;
  
    // declarations 
    move_t moves[MAX_MOVES];
    int n_moves;

    int j = np;
    result_t* buffer = NULL;
    int pp = 0;
    int* esclave; // pour savoir quel esclave fait quelle tache, on en a besoin pour recuperer moves[i]

    result->score = -MAX_SCORE - 1;
    result->pv_length = 0;
        
    if (test_draw_or_victory(T, result))
      return;

    compute_attack_squares(T);
  
    /* profondeur max atteinte ? si oui, évaluation heuristique */
    if (T->depth == 0) {
      result->score = (2 * T->side - 1) * heuristic_evaluation(T);
      return;
    }
        
    n_moves = generate_legal_moves(T, &moves[0]);

    // Initialisation tableaux 
    buffer = malloc(n_moves * sizeof(result_t));
    esclave = malloc(n_moves * sizeof(int));

    for(int k = 0; k < n_moves; k++) {
      esclave[k] = -1;
    }
  
    /* absence de coups légaux : pat ou mat */
    if (n_moves == 0) {
      result->score = check(T) ? -MAX_SCORE : CERTAIN_DRAW;
      return;
    }
        
    if (ALPHA_BETA_PRUNING)
      sort_moves(T, n_moves, moves);
  
    int attributions;
  
    // S'il y a plus d'esclaves que de taches 
    if ( p - np > n_moves ) {
      attributions = n_moves - 1;
    } else {
      attributions = n_moves;
    }

    for (int i = 0; i < attributions; i++) {
      tree_t child;
      result_t child_result;
      play_move(T, moves[i], &child);
      if( j <= p - np ) {
	//si le nombre d'esclave utilisé est < nombre de processus disponibles 
	esclave[i] = j; //esclave de rang j fait la tache j

	//attribution de la tache 
	envoyer_arbre(&child, j, 10);
	j++;
      } else {
	// Le maitre attend la liberation d'un esclave 

	// On recupere d'abord la source car on ne veut
	// pas recevoir le resultat d'un esclave qui traite un noeud d'un
	// autre appel
	int s;

	int  trouv = 0;
	while(trouv != 1){
	  for(int h = np; h < p; h++) {
	    if ( h != 0) {
	      int flag;
	      MPI_Iprobe(h, MPI_ANY_TAG, MPI_COMM_WORLD,&flag,&status);
	      if(flag != 0){
		s = h;
		trouv = 1;
		break;
	      }
	    }
	  }
	}
	recevoir_resultat(s, &child_result);
	int source = status.MPI_SOURCE;
	// selon la source, voir ou doit etre stocké le resultat
	for(int k = 0; k < attributions; k++) {
	  if( esclave[k] == source ) {
	    pp = k;
	    break;
	  }
	}
	//stocker le resultat
	buffer[pp] = child_result;
	esclave[pp] = -1;
      

	esclave[i] = source;

	envoyer_arbre(&child, source, 10);
	
      }
    
    } // fin for 
  
    // Cas ou le maitre descend d'un niveau
  
    if ( attributions == n_moves - 1 ) {
      tree_t child;
      result_t child_result;
      //remettre à jour le nombre de processus disponibles
      np = np + (j-1);
      play_move(T, moves[n_moves-1], &child);
      evaluate(&child, &child_result);
      buffer[n_moves-1] = child_result;
    }

    for(int i = 0; i < attributions; i++ ) {
      if(esclave[i] != -1) {
	recevoir_resultat(esclave[i], &(buffer[i]));
	esclave[i] = -1;
      }
    }
  
   
  
    for( int i = 0; i < n_moves; i++ ) {
      result_t child_result = buffer[i];
      int child_score = -child_result.score;
    
      if (child_score > result->score) {
	result->score = child_score;
	result->best_move = moves[i];
	result->pv_length = child_result.pv_length + 1;
	for(int j = 0; j < child_result.pv_length; j++)
	  result->PV[j+1] = child_result.PV[j];
	result->PV[0] = moves[i];
      }
    
      if (ALPHA_BETA_PRUNING && child_score >= T->beta)
	break;
    
      T->alpha = MAX(T->alpha, child_score);
    }
  
    free(buffer);
    free(esclave);
  }
  else {
    // Esclave
    evaluate_seq( T, result);
    envoyer_resultat(result);
  }
}

void decide(tree_t * T, result_t *result)
{
  for (int depth = 1;; depth++) {
    T->depth = depth;
    T->height = 0;
    T->alpha_start = T->alpha = -MAX_SCORE - 1;
    T->beta = MAX_SCORE + 1;
    np = 1;
    
    printf("=====================================\n");
    evaluate(T, result);

    printf("depth: %d / score: %.2f / best_move : ", T->depth, 0.01 * result->score);
    print_pv(T, result);
                
    if (DEFINITIVE(result->score))
      break;
  }



  // Envoyer un message de fin aux esclaves 
  for( int i = 1; i < p; i++ ) {
    envoyer_arbre(NULL, i, 0);
  }
}

double my_gettimeofday(){
  struct timeval tmp_time;
  gettimeofday(&tmp_time, NULL);
  return tmp_time.tv_sec + (tmp_time.tv_usec * 1.0e-6L);
}

int main(int argc, char **argv)
{  
  tree_t root;
  result_t result;

  /* Init */

  MPI_Init(&argc,&argv);
  MPI_Comm_size(MPI_COMM_WORLD, &p);
  MPI_Comm_rank(MPI_COMM_WORLD,&rang);

  // Maitre

  if (rang == 0) {
 
    /* Chronometrage */
    double debut, fin;

    //debut du chronometrage 
    debut = my_gettimeofday();

    if (argc < 2) {
      printf("usage: %s \"4k//4K/4P w\" (or any position in FEN)\n", argv[0]);
      exit(1);
    }

    if (ALPHA_BETA_PRUNING)
      printf("Alpha-beta pruning ENABLED\n");

    if (TRANSPOSITION_TABLE) {
      printf("Transposition table ENABLED\n");
      init_tt();
    }
        
    parse_FEN(argv[1], &root);
    print_position(&root);
    printf("=====================================\n");
        
    decide(&root, &result);

    printf("depth: %d / score: %.2f / best_move : ", root.depth, 0.01 * result.score);
    print_pv(&root,&result);

    printf("\nDécision de la position: ");
    switch(result.score * (2*root.side - 1)) {
    case MAX_SCORE: printf("blanc gagne\n"); break;
    case CERTAIN_DRAW: printf("partie nulle\n"); break;
    case -MAX_SCORE: printf("noir gagne\n"); break;
    default: printf("BUG\n");
    }

    printf("Node searched: %llu\n", node_searched);
    /* fin du chronometrage */
    fin = my_gettimeofday();
    fprintf( stderr, "Temps total de calcul : %g sec\n",  fin - debut);
    fprintf( stdout, "%g\n", fin - debut);
    if (TRANSPOSITION_TABLE)
      free_tt();

  } else {
    // Esclave

    // reçoit l'arbre qu'il doit traiter du maître
    while(1) { 

      recevoir_arbre(&root);  
      if( status.MPI_TAG != 0 ) {
	evaluate( &root, &result );
      } else {
	break;
      }
    }
  }

  MPI_Finalize();
  return 0;
}
